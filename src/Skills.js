import React ,{ Component } from 'react';
import './Skills.css'
import './purecircle.css'
import {Switch , Link,Route} from 'react-router-dom';
import About from './About';
class Skills extends  Component {
    render(){
        return <div >
         <div ><Link   to="/"><span className="namee1">Pokuri Praveena</span></Link></div>
            <Switch>
           <Route exact path="/" component={About}/>
           </Switch>
        <div className="box1"> </div>
          <div className="flex-wrapper">
         <div className="single-chart">
         <svg viewBox="0 0 36 36" className="circular-chart orange">
         <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
       />
        <path className="circle"
        stroke-dasharray="70, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
       <text x="18" y="20.35" className="percentage"></text>
       </svg>
       </div>
       </div>
       <div className="html">html5</div>
      
       <div className="box2"></div>
       <div className="flex-wrapper1">
         <div className="single-chart1">
         <svg viewBox="0 0 36 36" className="circular-chart blue">
         <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
       />
        <path className="circle"
        stroke-dasharray="70, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
       <text x="18" y="20.35" className="percentage"></text>
       </svg>
       </div>
       </div>
       <div className="css">css3</div>
       <div className="box3">  </div>
        <div className="flex-wrapper2">
         <div className="single-chart2">
         <svg viewBox="0 0 36 36" className="circular-chart green">
         <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
       />
        <path className="circle"
        stroke-dasharray="60, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
       <text x="18" y="20.35" className="percentage"></text>
       </svg>
       </div>
       </div>
      <div className="javascript">javascript</div>
      <div className="box4"></div>
        <div className="flex-wrapper3">
         <div className="single-chart3">
         <svg viewBox="0 0 36 36" className="circular-chart yellow">
         <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
       />
        <path className="circle"
        stroke-dasharray="60, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
       <text x="18" y="20.35" className="percentage"></text>
       </svg>
       </div>
       </div>
       <div className="reactjs">react js</div>
       <div className="box5"></div>
        <div className="flex-wrapper4">
         <div className="single-chart4">
         <svg viewBox="0 0 36 36" className="circular-chart lightgrean">
         <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
       />
        <path className="circle"
        stroke-dasharray="70, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
       <text x="18" y="20.35" className="percentage"></text>
       </svg>
       </div>
       </div>
       <div className="nodejs">node js</div>
       <div className="box6"></div>
       <div className="flex-wrapper5">
         <div className="single-chart5">
         <svg viewBox="0 0 36 36" className="circular-chart red">
         <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
       />
        <path className="circle"
        stroke-dasharray="40, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
       <text x="18" y="20.35" className="percentage"></text>
       </svg>
       </div>
       </div>
       <div className="mongodb">mongoDB</div> 
       <div className="box7"></div> 
       <div className="flex-wrapper6">
         <div className="single-chart6">
         <svg viewBox="0 0 36 36" className="circular-chart lightblue">
         <path className="circle-bg"
          d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
       />
        <path className="circle"
        stroke-dasharray="50, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
       <text x="18" y="20.35" className="percentage"></text>
       </svg>
       </div>
       </div> 
       <div className="mysql">Bootstrap</div>  


  
        </div>
       
    }
}
export default Skills;