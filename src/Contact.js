import React, { Component } from 'react';
import './Contact.css'
import {Switch , Link,Route} from 'react-router-dom';
import About from './About';
import axios from "axios";
class Contact extends Component{
    constructor() {
        super();
    
        this.state = { name: "", email: "" , message:""};
      }
      sendMail() {
        axios.post('http://localhost:5000/send', {
          name: this.state.name,
          email: this.state.email,
          message:this.state.message
        }).then((res) => {
          this.setState({name:""});
          this.setState({email:""});
          this.setState({message:""})
          console.log(res);
        })
      }
    


    render(){
        return <div className="main">
            <div ><Link   to="/"><span className="name3">Pokuri Praveena</span></Link></div>
            <Switch>
           <Route exact path="/" component={About}/>
           </Switch>
         
           {/* <form method="POST" action="send"> */}
            <input className="input1" type="text" placeholder="name" required value={this.state.name} onChange={(ev) => { this.setState({ name: ev.target.value }) }}/>
            {/* <span><img className="linkedin" src={require('./images/linkedin.svg')}/><span className="linkedinname"> linkedin</span></span> */}
            <input className="input2" type="text" placeholder="email"  required value={this.state.email} onChange={(ev) => { this.setState({ email: ev.target.value }) }}  />
            <span><img className="github" src={require('./images/github.svg')}/><span className="githubnames"> github</span></span>
            {/* <input className="input3" type="textarea" placeholder="message" ></input> */}
            <textarea rows="4" cols="50" name="comment" form="usrform" className="input3" placeholder="message" required value={this.state.message} onChange={(ev) => { this.setState({ message: ev.target.value }) }}></textarea>
            <span><img className="facebook" src={require('./images/facebook.svg')}/><span className="facebookname"> facebook</span></span>
            <button className="input4" onClick={() => this.sendMail()}><h6>send</h6></button>
            <span><img className="twiter" src={require('./images/twiter.svg')}/><span className="twitername"> twiter</span></span>
            <div><a  className="emailid" href="mailto:pravee.p533@gmail.com"><img className="email1" src={require('./images/mailbox.svg')}/><img className="email1" src={require('./images/email.svg')}/><span className="mailid1">pravee.p533@gmail.com</span></a></div>
            <div><a href=" tel:+917780674135"><img className="telephone1" src={require('./images/telephone.svg')}/><p className="telephonenum1">9742653898</p></a></div>
            {/* </form> */}
        </div>
    }
}
export default Contact;