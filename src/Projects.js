import React , { Component} from 'react';
import './Projects.css';
import {Switch , Link,Route} from 'react-router-dom';
import About from './About';
class Projects extends Component{
    render(){
        return <div className="parent3">
         <div ><Link   to="/"><span className="name5">Pokuri Praveena</span></Link></div>
            <Switch>
           <Route exact path="/" component={About}/>
           </Switch>
        <div className="project1">
        <div><img className="pro1" src={require('./images/hotel-booking.090a237a.jpg')}/>
        {/* <div className="bootstrap">Bootstrap template</div> */}
        </div>
        <div className="proname1"> ONLINE RESTAURANT WEBSITE </div >
        <div className="description">Description</div>
  
        <div className="para" > this websitewill aggregate table bookings in a restaurent from multiple providers and user can choose restaurent based on reviews.User can choose the restaurent with preferred table, based on the reviews and also get the rewards and discounts for booking and all these data will be stored in user activities.</div>
        <div className="stack">stack</div>
        <div className="list">Bootstrap, CSS, Html, Javascript</div>
        {/* <div><img className="githubb" src={require('./images/github.svg')}/><span className="gitname">view on github</span> </div> */}
        </div>
        <div className="project2">
        <div><img className="pro2" src={require('./images/add-previewtool.png')}/></div>
        <div className="proname2">Brand Tracker</div>
        <div className="description1">description</div>
        <div className="para1" >Brand Tracker gives the overall market share, profit and loss of products in different views like statically, grid, dashboard and table view. It compares the shares with the diﬀerent competitors.</div>
        <div className="stack1">stack</div>
        <div className="list1">Bootstrap, CSS, Html, Javascript</div>
        {/* <div><img className="githubb1" src={require('./images/github.svg')}/><span className="gitname1">view on github</span> </div> */}
        </div>
    </div>
    }

}
export default Projects;