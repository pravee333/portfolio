import React, { Component } from 'react';
import {Switch , Link,Route} from 'react-router-dom';
import Skills from './Skills';
import About from './About';
import Projects from './Projects';
import Contact from './Contact';
import './portfolio.css'
class Portfolio extends Component{
  //    handleClick() {
  //       var menuBox = document.getElementById('menu-box');    
  // if(menuBox.style.display == "none") { // if is menuBox displayed, hide it
  //   menuBox.style.display = "block";
  // }
  // else { // if is menuBox hidden, display it
  //   menuBox.style.display = "none";
  // }

  //     }
    
    render(){
        return<div>
          {/* <div class="pos-f-t">
  <div class="collapse" id="navbarToggleExternalContent">
    <div class="bg-dark p-4">
      <h5 class="text-white h4">Collapsed content</h5>
      <span class="text-muted">Toggleable via the navbar brand.</span>
    </div>
  </div>
  <nav class="navbar navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="true" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </nav>
</div>
           <i className="material-icons" onClick={(e) => this.handleClick(e)}>menu</i>
           <div id="menu-box" style={{display:"none"}}>
  <div   ><Link   to="/">About</Link></div>
            <div   ><Link to="/Skills">Skills</Link></div>
            <div    ><Link  to="/projects">Projects</Link></div>
            <div   ><Link  to="/contact">Contact</Link></div>
</div> */}


<div className="navbar1">
 
<nav class="navbar navbar-expand-lg navbar-light bg-light" >
  <a class="navbar-brand"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown" >
    <ul class="navbar-nav" >
      <li class="nav-item active"  >
        <a class="nav-link"><Link   to="/" ><span data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">About</span></Link><span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item" >
        <a class="nav-link"><Link   to="/Skills" ><span data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">Skills</span></Link></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" ><Link   to="/Projects"><span data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">Projects</span></Link></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" ><Link   to="/Contact"><span data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">Contact</span></Link></a>
      </li>
    </ul>
  </div>
</nav>
</div>
<div className="grid">
          <div  className="row">
               <div   className="col-sm-3" ><Link   to="/">About</Link></div>
            <div  className="col-sm-3" ><Link to="/Skills">Skills</Link></div>
            <div   class="col-sm-3" ><Link  to="/projects">Projects</Link></div>
            <div  className="col-sm-3" ><Link  to="/contact">Contact</Link></div>
            </div>
           </div>

           <Switch>
           <Route exact path="/" component={About}/>
            <Route exact path="/Skills" component={Skills}/>
            <Route exact path="/projects" component={Projects}/>
            <Route exact path="/contact" component={Contact}/>
            </Switch>
            {/* <div className="grid">
          <div  className="row">
               <div   className="col-sm-3" ><Link   to="/">About</Link></div>
            <div  className="col-sm-3" ><Link to="/Skills">Skills</Link></div>
            <div   class="col-sm-3" ><Link  to="/projects">Projects</Link></div>
            <div  className="col-sm-3" ><Link  to="/contact">Contact</Link></div>
            </div>
           </div>
            */}
           
        </div>
    }
}
export default Portfolio;